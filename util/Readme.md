#Overview
The Util project contains classes and traits that define common functionality that is generally useful and that is not directly related to the processing of bytecode.

#Compilation Order
This project contains macros which are used by OPAL and which need to be compiled/need to be available before we can compile the other projects.