#Overview
The VALIDATE project contains OPAL's integration tests and the specification of OPAL's architecture. The latter is validated when the tests are run.